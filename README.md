# tennis-cpp

Tennis kata using C++

First failing test

<script src="https://gitlab.com/cskarl-katas/tennis-cpp/snippets/1867472.js"></script>


Quack

<script src="https://gitlab.com/snippets/1867492.js"></script>

```c++
#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#define DOCTEST_CONFIG_SUPER_FAST_ASSERTS

#include <doctest.h>
#include <string>

TEST_CASE("a new game") {
  std::string actual = "";
  std::string expected = "Love All";
  CHECK(expected == actual);
}
```

Makefile (so I can autocmd vim to make after every save)

<script src="https://gitlab.com/cskarl-katas/tennis-cpp/snippets/1867474.js"></script>

```Makefile
tennis: tennis-tests.cpp
  g++ -std=c++17 -I. -o $@ $<
  tennis
```

Result

<script src="https://gitlab.com/snippets/1867492.js"></script>


